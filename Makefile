build:
	gcc main.c -o main -static -Os -s
	echo >> main
	cat main.sh | sed "s/^/=>LINE=>/g" >> main

terminal:
	gcc main.c -o main -static -Os -s -DTERMINAL
	echo >> main
	cat main.sh | sed "s/^/=>LINE=>/g" >> main

clean:
	rm -f main
