#include <stdlib.h>
#include <string.h>
#define APP "x-terminal-emulator"
int main(int argc, char* argv[]){
    char cmd[1024];
    strcpy(cmd,"");
    strcat(cmd,"cat ");
    strcat(cmd,argv[0]);
    strcat(cmd,"| grep -a -e '^=>LINE=>' | sed 's|^=>LINE=>||g' > /tmp/");
    strcat(cmd,argv[0]);
    system(cmd);

    strcpy(cmd,"");
#ifdef TERMINAL
    strcat(cmd,APP);
    strcat(cmd," -e \"");
#endif
    strcat(cmd,"bash '/tmp/");
    strcat(cmd,argv[0]);
    strcat(cmd,"'");
#ifdef TERMINAL
    strcat(cmd,"\"");
#endif
    return system(cmd);
}
